﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MVCSite
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            /*routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );*/

            routes.MapRoute(
                name: "Ajax",
                url: "ajax/{action}",
                defaults: new { controller = "Ajax", action = "Index", pageName = UrlParameter.Optional }
            );

            /*routes.MapRoute(
                name: "CMS",
                url: "cms/{action}",
                defaults: new { controller = "Cms", action = "Index", pageName = UrlParameter.Optional }
            );*/

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                 name: "Blog",
                 url: "blog/{action}/{id}",
                 defaults: new { controller = "Blog", action = "Index", id = UrlParameter.Optional }
             );

            routes.MapRoute(
                name: "Website",
                url: "{pageName}/{action}",
                defaults: new { controller = "Website", action = "Index", pageName = UrlParameter.Optional },
                constraints: new { pageName = new Classes.IsNotCMSRouteContraint() }
            );
        }
    }
}

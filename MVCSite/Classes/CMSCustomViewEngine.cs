﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCSite.Classes
{
    public class CMSCustomViewEngine : RazorViewEngine
    {
        /// <summary>
        /// Custom view engine for managing correct 
        /// </summary>

        public CMSCustomViewEngine()
        {
            try
            {
                List<string> NewPartialViewFormats = new List<string>();
                List<string> NewViewFormats = new List<string>();

                NewPartialViewFormats.Add("~/Views/Cms/Master/{0}.cshtml");
                var custom_website_folders = System.IO.Directory.GetDirectories(HttpContext.Current.Server.MapPath("~/Views/Website"));

                foreach (string custom_website_folder in custom_website_folders)
                {
                    try
                    {
                        DirectoryInfo di = new DirectoryInfo(custom_website_folder);

                        NewPartialViewFormats.Add("~/Views/Website/" + di.Name + "/Master/{0}.cshtml");
                        NewViewFormats.Add("~/Views/Website/" + di.Name + "/{0}.cshtml");
                    }

                    catch { }
                }

                base.PartialViewLocationFormats = base.PartialViewLocationFormats.Union(NewPartialViewFormats).ToArray();
                base.ViewLocationFormats = base.ViewLocationFormats.Union(NewViewFormats).ToArray();
            }

            catch { }
        }
    }
}
﻿using System.Web;
using System.Web.Routing;

namespace MVCSite.Classes
{
    public class IsNotCMSRouteContraint : IRouteConstraint
    {
        public bool Match (HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            try
            {
                if (httpContext.Request.RawUrl.ToLower().Contains("/cms"))
                    return false;

            }

            catch { }

            return true;
        }
    }
}
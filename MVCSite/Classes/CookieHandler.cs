﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

public class CookieHandler
{
    private HttpRequest request = null;
    private HttpResponse response = null;

    public CookieHandler(HttpRequest request, HttpResponse response)
    {
        this.response = response;
        this.request = request;
    }

    public void SetValue(string name, string value)
    {
        if (request != null && response != null)
        {
            if (request.Cookies.AllKeys.ToList().Contains(name))
                response.Cookies[name].Value = value;

            else
                response.Cookies.Add(new System.Web.HttpCookie(name, value));
        }
    }

    public void SetValue(string name, string value, DateTime expires)
    {
        if (request != null && response != null)
        {
            if (request.Cookies.AllKeys.ToList().Contains(name))
                response.Cookies[name].Value = value;

            else
            {
                var cookie = new System.Web.HttpCookie(name, value);
                cookie.Expires = expires;

                response.Cookies.Add(cookie);
            }
        }
    }

    public List<string> GetAllKeys()
    {
        try

        {
            if (request != null)
            {
                return request.Cookies.AllKeys.ToList();
            }
        }

        catch { }

        return new List<string>();
    }

    public bool HasKey(string name)
    {
        return (request != null && request.Cookies != null && request.Cookies.AllKeys.ToList().Contains(name)) ? true : false;
    }

    public void RemoveKey(string name)
    {
        if (HasKey(name))
        {
            response.Cookies[name].Expires = DateTime.Now.AddDays(-1);
            response.Cookies[name].Value = String.Empty;
        }
    }

    public string GetValue(string name)
    {
        if (HasKey(name) && request.Cookies != null)
            return request.Cookies[name].Value;

        return String.Empty;
    }
}

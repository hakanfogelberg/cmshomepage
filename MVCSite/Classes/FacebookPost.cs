﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Facebook;

namespace MVCSite.Classes
{
    public class FacebookHandler
    {
        public static FacebookPosts GetPosts()
        {
            try
            {
                HttpWebRequest a = HttpWebRequest.CreateHttp("https://graph.facebook.com/v2.9/oauth/access_token?client_id=119886031985932&client_secret=f4faec8158ebfbe03337c2eed1d37430&grant_type=client_credentials");

                string accessTokenJson = (new StreamReader(a.GetResponse().GetResponseStream())).ReadToEnd();
                var accessTokenInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<AccessTokenInfo>(accessTokenJson);

                HttpWebRequest r = HttpWebRequest.CreateHttp("https://graph.facebook.com/v2.9/1578355245823536?access_token=" + accessTokenInfo.access_token + "&fields=id,name,posts{message,type,created_time}");

                string content = (new StreamReader(r.GetResponse().GetResponseStream())).ReadToEnd();
                return Newtonsoft.Json.JsonConvert.DeserializeObject<FacebookPosts>(content);
            }

            catch { }

            return new FacebookPosts();
        }
    }

    public class AccessTokenInfo
    {
        public string access_token { get; set; }
        public string token_type { get; set; }

    }

    public class FacebookPosts
    {
        public string id { get; set; }
        public string name { get; set; }
        public FacebookPostsData posts { get; set; }

    }

    public class FacebookPostsData {
        public FacebookPost[] data { get; set; }
    }

    public class FacebookPost
    {
        public string ID { get; set; }
        public string Message { get; set; }
        public string Type { get; set; }

        [JsonProperty("created_time")]
        public string CreatedTime { get; set; }
    }
}
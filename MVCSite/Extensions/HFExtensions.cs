﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public static class HFExtensions
{
    public static int ToInt(this string str)
    {
        int i = 0;
        Int32.TryParse(str, out i);

        return i;
    }

    public static float ToFloat(this string str)
    {
        float i = 0;
        float.TryParse(str, out i);

        return i;
    }

    public static char ToChar(this string str, int pos)
    {
        if (str != null && str.Length >= pos + 1)
            return (char)str[pos];

        return ' ';
    }

    public static bool ToBoolean(this string str)
    {
        if (str != null && str.ToLower() == true.ToString().ToLower())
            return true;

        return false;
    }

    public static bool ToBooleanFromHtmlCheckBox(this string str)
    {
        return (str != null && str.ToLower() == "checked" || str != null && str.ToLower() == "on") ? true : false;
    }

    public static int ToBooleanBit(this bool cond)
    {
        return (cond) ? 1 : 0;
    }

    public static bool FromBooleanBit(this int cond)
    {
        return (cond == 1) ? true : false;
    }

    public static bool FromBooleanBit(this int? cond)
    {
        try
        {
            return (cond.HasValue && cond.Value == 1) ? true : false;
        }

        catch (Exception) { }

        return false;
    }

    public static DateTime ToDateTime(this string datatime_str)
    {
        DateTime toReturn = DateTime.MinValue;

        if (datatime_str != null)
            DateTime.TryParse(datatime_str, out toReturn);

        return toReturn;
    }
}

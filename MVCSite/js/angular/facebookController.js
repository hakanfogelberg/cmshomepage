﻿var homepageapp = angular.module('homepageapp', []);

homepageapp.controller('FacebookController', function FacebookController($scope, $http) {
    $scope.statusitems = {};

    $http({
        method: 'GET',
        url: '/ajax/facebookstatusposts'
    }).then(function successCallback(response) {
        $scope.statusitems = response.data
    }, function errorCallback(response) {
    });
});
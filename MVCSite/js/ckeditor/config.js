/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	
	config.removeButtons = 'Subscript,Superscript';

	// Make dialogs simpler.
	config.removeDialogTabs = 'image:advanced;link:advanced';
	config.removePlugins = 'save, newpage, preview, print, templates, flash, smiley, pagebreak, iframe, font, forms, colorbutton';
	config.entities = false;
	config.allowedContent = true; // Denna är fruktansvärt viktig, stänger av filtrering av html kod. Annars blir h2 etc extremt fel samt iframe fungerar inte etc.
	config.enterMode = CKEDITOR.ENTER_DIV;
	config.AutoParagraph = false;
};

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DataAccess;
using DataAccess.Sql;

namespace MVCSite.Repository
{
    public interface ICmsRepository
    {
        DataAccess.Sql.Page GetPage(int pageID);
        bool UpdatePage(DataAccess.Sql.Page page, bool isNew = true);
        void AddParagraph(int pageID, string paragraphHeader);
        Paragraph GetParagraph(int paragraphID);
        bool UpdateParagraph(Paragraph paragraph);
        void SetCurrentArea(int areaID);
    }

    public class CmsRepository : ICmsRepository
    {
        public SqlDBContext SqlDBContext { get; set; }

        #region -- Constructors

        public CmsRepository()
        {
            SqlDBContext = new SqlDBContext();
        }

        #endregion

        public DataAccess.Sql.Page GetPage(int pageID)
        {
            return (from p in SqlDBContext.Pages where p.PageID == pageID select p).FirstOrDefault() ?? new DataAccess.Sql.Page();
        }

        public void AddParagraph(int pageID, string paragraphHeader)
        {
            if (!(from p in SqlDBContext.Paragraphs where p.ParagraphHeader.ToLower() == paragraphHeader.ToLower() select p).Any())
            {
                Paragraph paragraph = new Paragraph {
                    ParagraphOrder = 0,
                    ParagraphPageID = pageID,
                    ParagraphHeader = paragraphHeader,
                    ParagraphText = string.Empty
                };

                SqlDBContext.Paragraphs.Add(paragraph);
                SqlDBContext.SaveChanges();
            }
        }

        public bool UpdateParagraph(Paragraph paragraph)
        {
            if (paragraph != null && paragraph.ParagraphID > -1)
            {
                if ((from p in SqlDBContext.Paragraphs where p.ParagraphID == paragraph.ParagraphID select p).Any())
                {
                    var pa = (from p in SqlDBContext.Paragraphs where p.ParagraphID == paragraph.ParagraphID select p).FirstOrDefault();

                    if(pa != null)
                    {
                        pa.ParagraphHeader = paragraph.ParagraphHeader;
                        pa.ParagraphText = paragraph.ParagraphText;

                        SqlDBContext.SaveChanges();
                        return true;
                    }
                }
            }

            return false;
        }

        public Paragraph GetParagraph(int paragraphID)
        {
            if(paragraphID > -1)
                return (from p in SqlDBContext.Paragraphs where p.ParagraphID == paragraphID select p).FirstOrDefault() ?? new Paragraph();

            return new Paragraph();
        }

        public static List<DataAccess.Sql.Paragraph> GetParagraphsForPage(int pageID)
        {
            if (pageID > -1)
            {
                SqlDBContext sql_context = new SqlDBContext();
                return (from pp in sql_context.Paragraphs where pp.ParagraphPageID == pageID select pp).ToList() ?? new List<DataAccess.Sql.Paragraph>();
            }

            return new List<Paragraph>();
        }

        public bool UpdatePage(DataAccess.Sql.Page page, bool isNew = true)
        {
            // First check that the user does not already exists. 
            if (isNew && !(from p in SqlDBContext.Pages where p.PageName == page.PageName && p.PageAreaID == page.PageAreaID select p).Any())
            {
                page.CreationDate = page.UpdatedDate = DateTime.Now;

                SqlDBContext.Pages.Add(page);
                SqlDBContext.SaveChanges();

                return true;
            }

            // else
            else
            {
                var p = (from pq in SqlDBContext.Pages where pq.PageID == page.PageID select pq).FirstOrDefault();

                if (p != null)
                {
                    p.PageAreaID = page.PageAreaID;
                    p.PageName = page.PageName;
                    p.UpdatedDate = DateTime.Now;
                    p.IsStartPage = page.IsStartPage;

                    SqlDBContext.SaveChanges();
                    return true;
                }
            }

            return false;

        }

        public static List<Area> GetAllWebsites()
        {
            try
            {
                SqlDBContext sql_context = new SqlDBContext();
                return (from a in sql_context.Areas select a).ToList() ?? new List<Area>();
            }

            catch { }

            return new List<Area>();
        }

        public void SetCurrentArea(int areaID)
        {
            if (areaID > -1)
            {
                var area = (from a in SqlDBContext.Areas where a.AreaID == areaID select a).FirstOrDefault();

                if (area != null)
                    SetCurrentArea(area);

                else
                {
                    area = (from a in SqlDBContext.Areas select a).FirstOrDefault();

                    if (area != null)
                        SetCurrentArea(area);
                }
            }

            else
            {
                var area = (from a in SqlDBContext.Areas select a).FirstOrDefault();

                if (area != null)
                    SetCurrentArea(area);
            }
        }

        public static void SetCurrentArea(Area area)
        {
            try
            {
                HttpContext.Current.Session[Resources.GlobalSettings.GetCurrentAreaCookieName] = area;
            }

            catch { }
        }

        public static Area GetCurrentArea()
        {
            try
            {
                if (HttpContext.Current.Session[Resources.GlobalSettings.GetCurrentAreaCookieName] != null)
                    return (Area)HttpContext.Current.Session[Resources.GlobalSettings.GetCurrentAreaCookieName];
            }

            catch { }

            return new Area();
        }

        public static List<DataAccess.Sql.Page> GetAllPagesForArea(int areaID = -1)
        {
            try
            {
                SqlDBContext sql_context = new SqlDBContext();

                return (areaID == -1) 
                    ? new List<DataAccess.Sql.Page> { (from p in sql_context.Pages select p).FirstOrDefault() ?? new DataAccess.Sql.Page() }
                    : (from p in sql_context.Pages where p.PageAreaID == areaID select p).ToList() ?? new List<DataAccess.Sql.Page>();
            }

            catch { }

            return new List<DataAccess.Sql.Page>();
        }
    }
}
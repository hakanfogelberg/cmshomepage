﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DataAccess;
using DataAccess.Sql;

namespace MVCSite.Repository
{
    public interface IAreaRepository
    {
        Area GetCurrentAreaForPage();
        bool AddArea(string areaName, string domainBindings);
        bool IsMongoDbArea(Area a);
    }

    public class AreaRepository : IAreaRepository
    {
        SqlDBContext sql_context = null;

        #region -- Constructors

        public AreaRepository()
        {
            sql_context = new SqlDBContext();
        }

        #endregion

        public Area GetCurrentAreaForPage()
        {
            try
            {
                if (HttpContext.Current.Request != null && HttpContext.Current.Request.RawUrl != null)
                {
                    string rawUrl = HttpContext.Current.Request.Url.AbsoluteUri;

                    // Right now there are only support for one domain. 
                    var area = (from a in sql_context.Areas where rawUrl.Contains(a.DomainBindings) select a).FirstOrDefault();

                    if (area != null)
                        return area;

                    else
                        return GetDefaultArea();
                }

                return null;
            }

            catch { }

            return null;
        }

        public bool IsMongoDbArea(Area a)
        {
            return (a != null && a.AreaName == Resources.GlobalSettings.AreaRepositoryDefaultAreaName);
        }
        
        public Area GetDefaultArea()
        {
            try
            {
                if (HttpContext.Current.Request != null && HttpContext.Current.Request.RawUrl != null)
                {
                    string rawUrl = HttpContext.Current.Request.RawUrl;

                    // Right now there are only support for one domain. 
                    var area = (from a in sql_context.Areas where a.AreaName == Resources.GlobalSettings.AreaRepositoryDefaultAreaName select a).FirstOrDefault();

                    if (area != null)
                        return area;
                }
            }

            catch { }

            return null;
        }

        public bool AddArea(string areaName, string domainBindings)
        {
            if(!string.IsNullOrEmpty(areaName) && !string.IsNullOrEmpty(domainBindings))
            {
                if(!(from a in sql_context.Areas where a.AreaName.ToLower() == areaName select a).Any())
                {
                    Area a = new Area
                    {
                        AreaName = areaName,
                        DomainBindings = domainBindings
                    };

                    sql_context.Areas.Add(a);
                    sql_context.SaveChanges();

                    return true;
                }
            }

            return false;
        }
    }
}
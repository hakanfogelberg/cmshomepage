﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DataAccess;
using DataAccess.Sql;
using System.ComponentModel;

namespace MVCSite.Repository
{
    public interface ICmsUserRepository
    {
        bool HasCmsUser(string username, string password);
        bool UpdateCmsUser(CMSUser user);
        CMSUser GetCmsUser(string username);
    }

    public class CmsUserRepository : ICmsUserRepository
    {
        SqlDBContext sql_context = null;

        #region -- Constructors

        public CmsUserRepository()
        {
            sql_context = new SqlDBContext();
        }

        #endregion

        public bool HasCmsUser(string username, string password)
        {
            var user = (from u in sql_context.CMSUser where u.UserName == username && u.Password == password select u).FirstOrDefault();

            if (user != null)
            {
                CookieHandler cookieHandler = new CookieHandler(HttpContext.Current.Request, HttpContext.Current.Response);
                cookieHandler.SetValue(Resources.GlobalSettings.CmsUserRepositoryLoginCookieName, user.CMSUserID.ToString(), DateTime.Now.AddHours(2));

                return true;
            }

            return false;
        }

        public bool UpdateCmsUser(CMSUser user)
        {
            try
            {
                if (user != null && !string.IsNullOrEmpty(user.UserName))
                {
                    // First check that the user does not already exists. 
                    if (!(from u in sql_context.CMSUser where u.UserName == user.UserName select u).Any())
                    {
                        sql_context.CMSUser.Add(user);
                        sql_context.SaveChanges();

                        return true;
                    }

                    // else
                    else
                    {
                        var u = (from uq in sql_context.CMSUser where uq.CMSUserID == user.CMSUserID select uq).FirstOrDefault();

                        if (u != null)
                        {
                            u.UserName = user.UserName;
                            u.Password = user.Password;
                            u.IsAdministrator = user.IsAdministrator;
                            u.Name = user.Name;
                            u.ImageUrl = user.ImageUrl;

                            sql_context.SaveChanges();
                            return true;
                        }
                    }
                }
            }

            catch { }

            return false;
        }

        public CMSUser GetCmsUser(string username)
        {
            try
            {
                if (!string.IsNullOrEmpty(username))
                {
                    var u = (from uq in sql_context.CMSUser where uq.UserName == username select uq).FirstOrDefault();

                    if (u != null)
                        return u;
                }
            }

            catch { }

            return new CMSUser();
        }

        [ReadOnly(true)]
        public static bool IsLoggedIn
        { 
            get
            {
                return (CurrentCmsUser != null) ? true  : false;
            }
        }

        [ReadOnly(true)]
        public static CMSUser CurrentCmsUser
        {
            get
            {
                try
                {
                    SqlDBContext sql_context = new SqlDBContext();

                    if (HttpContext.Current.Items[Resources.GlobalSettings.CmsUserRepositoryLoginCookieName + "_CurrentItem"] != null)
                        return (CMSUser)HttpContext.Current.Items[Resources.GlobalSettings.CmsUserRepositoryLoginCookieName + "_CurrentItem"];

                    CookieHandler cookieHandler = new CookieHandler(HttpContext.Current.Request, HttpContext.Current.Response);
                    string loginCookieInfo = cookieHandler.GetValue(Resources.GlobalSettings.CmsUserRepositoryLoginCookieName);

                    if (!string.IsNullOrEmpty(loginCookieInfo))
                    {
                        int userID = loginCookieInfo.ToInt();
                        var user = (from u in sql_context.CMSUser where u.CMSUserID == userID select u).FirstOrDefault();

                        if (user != null)
                        {
                            HttpContext.Current.Items[Resources.GlobalSettings.CmsUserRepositoryLoginCookieName + "_CurrentItem"] = user;
                            return user;
                        }
                    }
                }

                catch { }

                return null;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DataAccess;
using DataAccess.Sql;

namespace MVCSite.Repository
{
    public interface IPageRepository
    {
        void AddPageToAreaArea(Area a, DataAccess.Sql.Page p);
    }

    public class PageRepository : IPageRepository
    {
        SqlDBContext sql_context = null;

        #region -- Constructors

        public PageRepository()
        {
            sql_context = new SqlDBContext();
        }

        #endregion

        public void AddPageToAreaArea(Area a, DataAccess.Sql.Page p)
        {

        }
    }
}
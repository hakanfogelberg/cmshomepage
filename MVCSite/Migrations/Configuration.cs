namespace MVCSite.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using DataAccess;

    internal sealed class Configuration : DbMigrationsConfiguration<DataAccess.SqlDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            base.AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(DataAccess.SqlDBContext context)
        {
        }
    }
}

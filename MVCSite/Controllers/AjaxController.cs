﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using MVCSite.Classes;

namespace MVCSite.Controllers
{
    public class AjaxController : Controller
    {
        public ActionResult Index()
        {
            return Content("Error: Forbidden");
        }

        public JsonResult FacebookStatusPosts()
        {
            List<FacebookPost> fb_posts = new List<FacebookPost>();

            try
            {
                var posts = FacebookHandler.GetPosts();
                fb_posts = (posts != null && posts.posts != null && posts.posts.data != null && posts.posts.data.Count() > 0) ? posts.posts.data.Where(p => !string.IsNullOrEmpty(p.Message)).ToList() : new List<FacebookPost>();

                fb_posts.ForEach(f => f.CreatedTime = DateTime.Parse(f.CreatedTime).ToString("yyyy-MM-dd hh:mm:ss"));
                fb_posts = fb_posts.OrderByDescending(f => f.CreatedTime).Take(1).ToList();
            }

            catch { }
            
            return Json(fb_posts, JsonRequestBehavior.AllowGet);
        }
    }
}
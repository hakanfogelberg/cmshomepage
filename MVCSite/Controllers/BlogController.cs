﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccess;

namespace MVCSite.Controllers
{
    public class BlogController : Controller
    {
        // GET: Blog
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllPosts()
        {
            return PartialView("PartialBlogPost");
        }

        public ActionResult ViewBlogPost()
        {
            int blogID = 0;

            if (Request.QueryString["ID"] != null)
            {
                Int32.TryParse(Request.QueryString["ID"], out blogID);

                DataAccess.Blog blog = new DataAccess.BlogContext().GetBlogWithBlogID(blogID);
                return View("ViewBlogPost", blog);
            }

            return View("ViewBlogPost", new Blog { Header = "The blog could not be found." });
        }

        [HttpPost]
        public ActionResult Save()
        {
            try
            {
                if (Request.Form["header"] != null && Request.Form["ingress"] != null)
                {
                    DataAccess.BlogContext context = new DataAccess.BlogContext();

                    context.Save(new DataAccess.Blog
                    {
                        BlogID = (int)context.GetCount() + 1,
                        Header = Request.Form["header"].ToString(),
                        Ingress = Request.Form["ingress"].ToString(),
                        Message = (Request.Form["message"] != null) ? Request.Form["message"].ToString() : string.Empty,
                        Date = DateTime.Now
                    });
                }
            }

            catch { }

            return View("AddPost"); 
        }
    }
}
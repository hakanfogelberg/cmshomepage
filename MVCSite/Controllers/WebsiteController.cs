﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using DataAccess;
using MVCSite.Repository;

namespace MVCSite.Controllers
{
    public class WebsiteController : Controller
    {
        private IPageRepository pageRepo = null;
        private IAreaRepository areaRepo = null;

        #region -- Initiatiate repos

        public WebsiteController(IPageRepository pageRepo, IAreaRepository areaRepo)
        {
            this.pageRepo = pageRepo;
            this.areaRepo = areaRepo;
        }

        #endregion

        public ActionResult Index(string pageName)
        {
            var area = areaRepo.GetCurrentAreaForPage();

            // First check if the page are the default one created with MongoDB
            if (areaRepo.IsMongoDbArea(area))
            {
                if (!string.IsNullOrEmpty(pageName))
                {
                    PageContext context = new PageContext();
                    Page page = context.GetPageWithPageName(pageName);

                    if (page == null)
                        page = new Page { PageName = pageName, PageID = context.GetCount() + 1 };

                    ViewBag.CurrentPage = page;
                    return View("Hakan/Index", page);
                }

                else
                {
                    PageContext context = new PageContext();
                    var page = ViewBag.CurrentPage = context.GetStartPage();

                    return View("Hakan/Index", page);
                }
            }

            // Otherwise start creating the page form the page and paragraphs content. 
            else
            {
                // This one should work diffrent later.
                return View(area.AreaName + "/Index");
            }
        }

        public ActionResult Edit(string pageName)
        {
            PageContext context = new PageContext();
            Page page = context.GetPageWithPageName(pageName);

            if (page == null)
                page = new Page { PageName = pageName, PageID = context.GetCount() + 1 };

            ViewBag.CurrentPage = page;
            return View("Hakan/Edit", page);
        }

        [HttpPost]
        public JsonResult Delete()
        {
            if (Request.Form["pageid"] != null)
            {
                int pageid = 0;
                Int32.TryParse(Request.Form["pageid"], out pageid);

                PageContext context = new PageContext();
                Page page = context.GetPageWithPageID(pageid);

                if(page != null)
                {
                    context.Delete(page.PageID);
                    return Json(true);
                }
            }

            return Json(false);
        }

        [HttpPost]
        public JsonResult Save()
        {
            if(Request.Form["pageid"] != null && Request.Form["pagename"] != null && Request.Form["pagehtmlcode"] != null)
            {
                int pageid = 0;
                bool isStartpage = false;

                Int32.TryParse(Request.Form["pageid"], out pageid);

                if (Request.Form["pagehtmlcode"] != null)
                    isStartpage = (Request.Form["isstartpage"] == "1") ? true : false;

                PageContext context = new PageContext();
                Page page = context.GetPageWithPageID(pageid);

                if(page == null)
                    page = new Page { PageID = context.GetCount() + 1, CreationDate = DateTime.Now };

                page.PageName = HttpUtility.UrlDecode(Request.Form["pagename"]);
                page.HtmlCode = HttpUtility.UrlDecode(Request.Form["pagehtmlcode"]);
                page.LastUpdatedDate = DateTime.Now;
                page.IsStartPage = isStartpage;

                context.Save(page);
                return Json(false);
            }

            return Json(false);
        }
    }
}
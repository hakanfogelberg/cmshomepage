﻿using MVCSite.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MVCSite.Controllers
{
    public class CmsController : Controller
    {
        ICmsUserRepository userRepo = null;
        ICmsRepository cmsRepo = null;

        #region -- Initiate respository         

        public CmsController(ICmsUserRepository userRepo, ICmsRepository cmsRepo)
        {
            this.userRepo = userRepo;
            this.cmsRepo = cmsRepo;
        }

        #endregion

        [Route("cms")]
        public ActionResult Index(int areaID = -1)
        {
            cmsRepo.SetCurrentArea(areaID);
            return View();
        }

        [Route("cms/pages")]
        public ActionResult Pages(int areaID = -1)
        {
            return View("Index");
        }

        [Route("cms/editpage/{pageID}")]
        public ActionResult EditPage(int pageID)
        {
            if (pageID > -1)
            {
                var page = cmsRepo.GetPage(pageID);
                return View("Index", page);
            }

            return View("Index");
        }

        [Route("cms/addparagraph/{pageID}")]
        public ActionResult AddParagraph(int pageID)
        {
            try
            {
                if (Request.Form["addNewParagraphHeader"] != null)
                {
                    string addNewParagraphHeader = Request.Form["addNewParagraphHeader"];
                    cmsRepo.AddParagraph(pageID, addNewParagraphHeader);
                }
            }

            catch { }

            return Redirect("/cms/editpage/" + pageID);
        }

        [Route("cms/newpage/{areaID}")]
        public ActionResult NewPage(int areaID)
        {
            try
            {
                if(areaID > -1)
                {
                    cmsRepo.UpdatePage(new DataAccess.Sql.Page
                    {
                        IsStartPage = false,
                        PageName = "New page",
                        CreationDate = DateTime.Now,
                        UpdatedDate = DateTime.Now,
                        PageAreaID = areaID
                    });
                }
            }

            catch { }

            return View("Index");
        }

        [Route("cms/page/save/{pageID}")]
        public ActionResult SavePage(int pageID)
        {
            try
            {
                if (Request.Form["editpagenametext"] != null)
                {
                    string editpagenametext = Request.Form["editpagenametext"];
                    var page = cmsRepo.GetPage(pageID);

                    if(page != null)
                    {
                        page.PageName = editpagenametext;
                        page.UpdatedDate = DateTime.Now;
                        cmsRepo.UpdatePage(page, false);
                    }
                }
            }

            catch { }

            return View("Index");
        }

        [Route("cms/editparagraph/{paragraphID}")]
        public ActionResult EditParagraph(int paragraphID)
        {
            var paragraph = cmsRepo.GetParagraph(paragraphID);

            if (paragraph != null)
            {
                var page = cmsRepo.GetPage(paragraph.ParagraphPageID);

                if (page != null)
                {
                    ViewBag.CurrentParagraph = paragraph;
                    return View("Index", page);
                }
            }

            return View("Index");
        }

        [Route("cms/saveparagraph/{paragraphID}")]
        public ActionResult SaveParagraph(int paragraphID)
        {
            if (Request.IsAjaxRequest())
            {
                var paragraph = cmsRepo.GetParagraph(paragraphID);

                if (paragraph != null)
                {
                    var page = cmsRepo.GetPage(paragraph.ParagraphPageID);

                    if (page != null)
                    {
                        // Now we must also update the paragraph
                        if (Request.Form["currentparagraphheader"] != null && Request.Form["currentparagraphtext"] != null)
                        {
                            string currentparagraphheader = Server.UrlDecode(Request.Form["currentparagraphheader"]);
                            string currentparagraphtext = Server.UrlDecode(Request.Form["currentparagraphtext"]);

                            paragraph.ParagraphHeader = currentparagraphheader;
                            paragraph.ParagraphText = currentparagraphtext;
                            cmsRepo.UpdateParagraph(paragraph);

                            paragraph = cmsRepo.GetParagraph(paragraphID);
                        }

                        ViewBag.CurrentParagraph = paragraph;
                        return View("Index", page);
                    }
                }
            }

            return View("Index");
        }

        [Route("cms/login")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Login()
        {
            try
            {
                if (Request.Form["username"] != null && Request.Form["password"] != null)
                {
                    string username = Request.Form["username"];
                    string password = Request.Form["password"];

                    if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                    {
                        if (userRepo.HasCmsUser(username, password))
                        {
                            FormsAuthentication.SetAuthCookie(username, false);

                            var authTicket = new FormsAuthenticationTicket(1, username, DateTime.Now, DateTime.Now.AddHours(2), false, string.Empty);
                            string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                            var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

                            HttpContext.Response.Cookies.Add(authCookie);
                        }
                    }

                    else
                        ViewBag.IsNotValidLogin = true;
                }
            }

            catch { }

            return View("Index");
        }

        [Route("cms/logoff")]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return View("Index");
        }
    }
}
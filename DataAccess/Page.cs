﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Elasticsearch.Net;
using Elasticsearch.Net.Connection;
using MongoDB;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver.Builders;

namespace DataAccess
{
    public class PageContext : BaseDBContext
    {
        public PageContext() : base("HakanHomesite") {}

        public Page GetPageWithPageID(int id)
        {
            try
            {
                IMongoQuery query = Query<Page>.EQ(b => b.PageID, id);
                return Pages.Find(query).FirstOrDefault();
            }

            catch { }

            return new Page();
        }

        public Page GetPageWithPageName(string pageName)
        {
            try
            {
                IMongoQuery query = Query<Page>.Where(p => p.PageName.ToLower() == pageName.ToLower());//   .EQ(b => b.PageName, pageName.ToLower());
                return Pages.Find(query).FirstOrDefault();
            }

            catch { }

            return new Page();
        }

        public List<Page> GetAllPages()
        {
            return Pages.FindAll().ToList();
        }

        public Page GetStartPage()
        {
            try
            {
                IMongoQuery query = Query<Page>.Where(o => o.IsStartPage);
                Page p = Pages.Find(query).OrderByDescending(o => o.LastUpdatedDate).FirstOrDefault();

                if (p != null)
                    return p;
            }

            catch { }

            return Pages.FindOne();
        }

        public int GetCount()
        {
            return (int)Pages.Count();
        }

        public void Save(Page page)
        {
            try
            {
                if (page != null && page.PageID != -1)
                    this.Pages.Save(page);
            }

            catch { }
        }

        public void Delete(int pageId)
        {
            IMongoQuery query = Query<Page>.EQ(b => b.PageID, pageId);
            this.Pages.Remove(query);
        }

        public MongoDB.Driver.MongoCollection<Page> Pages
        {
            get
            {
                return this.mongoDatabase.GetCollection<Page>("Pages");
            }
        }
    }

    public class Page
    {
        [BsonId]
        public int PageID { get; set; }
        public string PageName { get; set; }
        public string HtmlCode { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastUpdatedDate { get; set; }
        public List<int> PuffIdList { get; set; } 
        public bool IsStartPage { get; set; }

        public Page()
        {
            PageID = -1;
            PageName = HtmlCode = string.Empty;
            PuffIdList = new List<int>();
            CreationDate = DateTime.MinValue;
            LastUpdatedDate = DateTime.MinValue;
        }
    }
}

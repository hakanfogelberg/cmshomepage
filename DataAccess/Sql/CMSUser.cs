﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace DataAccess.Sql
{
    public class CMSUser
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CMSUserID { get; set; }

        [Required]
        [MaxLength(4096)]
        public string UserName { get; set; }

        [Required]
        [MaxLength(4096)]
        public string Password { get; set; }

        [Required]
        [MaxLength(4096)]
        public string Name { get;set; }

        [MaxLength(4096)]
        public string ImageUrl { get; set; }

        [MaxLength(4096)]
        [RegularExpression(@"^$|[\w0-9._%+-]+@[\w0-9.-]+\.[\w]{2,6}")]
        public string Email { get; set; }

        [DefaultValue(false)]
        public bool IsAdministrator { get; set; }

        public ICollection<CMSUserAreaRights> CMSUserAreaRights { get; set; }
    }
}

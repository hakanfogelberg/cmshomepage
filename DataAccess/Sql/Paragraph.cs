﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace DataAccess.Sql
{
    public class Paragraph
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ParagraphID { get; set; }

        [Required]
        public int ParagraphPageID { get; set; }

        [MaxLength(4096)]
        public string ParagraphHeader { get; set; }

        [MaxLength(2048)]
        public string ParagraphText { get; set; }

        [DefaultValue(0)]
        public int ParagraphOrder { get; set; }
    }
}

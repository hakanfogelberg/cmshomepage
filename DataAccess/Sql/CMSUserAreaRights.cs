﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.Sql
{
    public class CMSUserAreaRights
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CMSUserAreaRightsID { get; set; }

        [Required]
        public int UserID { get; set; }

        [Required]
        public int AreaID { get; set; }
    }
}

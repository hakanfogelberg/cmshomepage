﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Elasticsearch.Net;
using Elasticsearch.Net.Connection;
using MongoDB;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver.Builders;


namespace DataAccess
{
    public class PuffContext : BaseDBContext
    {
        public PuffContext() : base("HakanHomesite") { }

        public Puff GetPuffWithPuffID(int id)
        {
            try
            {
                IMongoQuery query = Query<Page>.EQ(b => b.PageID, id);
                return Puffs.Find(query).FirstOrDefault();
            }

            catch { }

            return new Puff();
        }

        public List<Puff> GetAllPuffs()
        {
            return Puffs.FindAll().ToList();
        }

        public void Save(Puff puff)
        {
            try
            {
                if (puff != null && puff.PuffID != -1)
                    this.Puffs.Save(puff);
            }

            catch { }
        }

        public MongoDB.Driver.MongoCollection<Puff> Puffs
        {
            get
            {
                return this.mongoDatabase.GetCollection<Puff>("Puffs");
            }
        }

    }

    public class Puff
    {
        [BsonId]
        public int PuffID { get; set; }
        public string HtmlCode { get; set; }
        public string ImageUrl { get; set; }
    }
}

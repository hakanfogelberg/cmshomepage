﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

public static class HFExtensions
{
    #region --Datatype validation

    public static bool IsInteger(this string str)
    {
        Int64 o;
        return Int64.TryParse(str, out o);
    }

    public static bool IsNumeric(this string str)
    {
        float o;
        return float.TryParse(str, out o);
    }

    public static bool IsOneCharacter(this string str)
    {
        return (str != null && str.Length == 1) ? true : false;
    }

    public static bool IsValidEmail(this string str)
    {
        if (str != null)
        {
            Regex reqex = new Regex(@"[\w0-9._%+-]+@[\w0-9.-]+\.[\w]{2,6}");
            return reqex.IsMatch(str);
        }

        return false;
    }

    #endregion
}

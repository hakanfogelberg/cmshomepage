﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Elasticsearch.Net;
using Elasticsearch.Net.Connection;
using MongoDB;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver.Builders;

/// <summary>
/// 
/// </summary>

namespace DataAccess
{
    public class BaseDBContext
    {
        protected MongoClient mongoClient;
        protected MongoServer mongoServer;
        protected MongoDB.Driver.MongoDatabase mongoDatabase;

        public BaseDBContext()
        {
            //mongoClient = new MongoClient("mongodb://192.168.1.130");
            mongoClient = new MongoClient("mongodb://127.0.0.1");
            mongoServer = mongoClient.GetServer();
        }

        public BaseDBContext(string dbName) 
            : this()
        {
            mongoDatabase = mongoServer.GetDatabase(dbName);
        }
    }
}

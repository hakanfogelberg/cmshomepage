﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Elasticsearch.Net;
using Elasticsearch.Net.Connection;
using MongoDB;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver.Builders;

namespace DataAccess
{
    public class BlogContext : BaseDBContext
    {
        public BlogContext() : base("HakanHomesite") { }

        public Blog GetBlogWithBlogID(int id)
        {
            try
            {
                IMongoQuery query = Query<Blog>.EQ(b => b.BlogID, id);
                return Blogs.Find(query).FirstOrDefault();
            }

            catch { }

            return new Blog();
        }

        public bool HasBlog(int id)
        {
            try
            {
                IMongoQuery query = Query<Blog>.EQ(b => b.BlogID, id);
                return (Blogs.Find(query).FirstOrDefault() != null) ? true : false;
            }

            catch { }

            return false;
        }

        public List<Blog> GetAllBlogs()
        {
            return Blogs.FindAll().ToList();
        }

        public int GetCount()
        {
            return (int)this.Blogs.Count();
        }

        public void Save(Blog blog)
        {
            try
            {
                if (blog != null && blog.BlogID != -1)
                    this.Blogs.Save(blog);
            }

            catch { }
        }

        public MongoDB.Driver.MongoCollection<Blog> Blogs
        {
            get
            {
                return this.mongoDatabase.GetCollection<Blog>("Blogs");
            }
        }

        public MongoDB.Driver.MongoDatabase BlogDatabase
        {
            get
            {
                return mongoDatabase;
            }
        }
    }

    public class Blog
    {
        [BsonId]
        public int BlogID { get; set; }
        public string Header { get; set; }
        public string Ingress { get; set; }
        public string Message { get; set; }
        public DateTime Date { get; set; }

        public Blog()
        {
            Header = Ingress = Message = string.Empty;
            Date = DateTime.MinValue;
            BlogID = -1;
        }
    }
}

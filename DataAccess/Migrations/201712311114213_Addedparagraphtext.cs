namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addedparagraphtext : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Paragraph", "ParagraphHeader", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Paragraph", "ParagraphHeader");
        }
    }
}

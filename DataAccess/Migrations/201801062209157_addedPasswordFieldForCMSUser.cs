namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedPasswordFieldForCMSUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CMSUser", "Password", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CMSUser", "Password");
        }
    }
}

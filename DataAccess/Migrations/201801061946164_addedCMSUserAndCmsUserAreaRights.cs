namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedCMSUserAndCmsUserAreaRights : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Paragraph", "ParagraphOrder", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Paragraph", "ParagraphOrder");
        }
    }
}

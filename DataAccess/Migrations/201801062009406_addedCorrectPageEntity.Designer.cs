// <auto-generated />
namespace DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.0-30225")]
    public sealed partial class addedCorrectPageEntity : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addedCorrectPageEntity));
        
        string IMigrationMetadata.Id
        {
            get { return "201801062009406_addedCorrectPageEntity"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

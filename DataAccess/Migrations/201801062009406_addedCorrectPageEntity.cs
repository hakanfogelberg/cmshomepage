namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedCorrectPageEntity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Page", "UpdatedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Paragraph", "Page_PageID", c => c.Int());
            AlterColumn("dbo.Page", "PageName", c => c.String(nullable: false));
            CreateIndex("dbo.Paragraph", "Page_PageID");
            AddForeignKey("dbo.Paragraph", "Page_PageID", "dbo.Page", "PageID");
            DropColumn("dbo.Page", "HtmlCode");
            DropColumn("dbo.Page", "LastUpdatedDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Page", "LastUpdatedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Page", "HtmlCode", c => c.String());
            DropForeignKey("dbo.Paragraph", "Page_PageID", "dbo.Page");
            DropIndex("dbo.Paragraph", new[] { "Page_PageID" });
            AlterColumn("dbo.Page", "PageName", c => c.String());
            DropColumn("dbo.Paragraph", "Page_PageID");
            DropColumn("dbo.Page", "UpdatedDate");
        }
    }
}

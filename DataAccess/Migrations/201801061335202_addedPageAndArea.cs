namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedPageAndArea : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Area",
                c => new
                    {
                        AreaID = c.Int(nullable: false, identity: true),
                        AreaName = c.String(),
                        DomainBindings = c.String(),
                    })
                .PrimaryKey(t => t.AreaID);
            
            CreateTable(
                "dbo.Page",
                c => new
                    {
                        PageID = c.Int(nullable: false, identity: true),
                        PageName = c.String(),
                        HtmlCode = c.String(),
                        CreationDate = c.DateTime(nullable: false),
                        LastUpdatedDate = c.DateTime(nullable: false),
                        IsStartPage = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.PageID);
            
            AddColumn("dbo.Paragraph", "ParagraphPageID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Paragraph", "ParagraphPageID");
            DropTable("dbo.Page");
            DropTable("dbo.Area");
        }
    }
}

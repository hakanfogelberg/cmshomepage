namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedPageAreIDToPage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Page", "PageAreaID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Page", "PageAreaID");
        }
    }
}

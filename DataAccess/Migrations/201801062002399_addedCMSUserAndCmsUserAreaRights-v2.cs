namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedCMSUserAndCmsUserAreaRightsv2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CMSUser",
                c => new
                    {
                        CMSUserID = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        ImageUrl = c.String(),
                        Email = c.String(),
                        IsAdministrator = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CMSUserID);
            
            CreateTable(
                "dbo.CMSUserAreaRights",
                c => new
                    {
                        CMSUserAreaRightsID = c.Int(nullable: false, identity: true),
                        UserID = c.Int(nullable: false),
                        AreaID = c.Int(nullable: false),
                        CMSUser_CMSUserID = c.Int(),
                    })
                .PrimaryKey(t => t.CMSUserAreaRightsID)
                .ForeignKey("dbo.CMSUser", t => t.CMSUser_CMSUserID)
                .Index(t => t.CMSUser_CMSUserID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CMSUserAreaRights", "CMSUser_CMSUserID", "dbo.CMSUser");
            DropIndex("dbo.CMSUserAreaRights", new[] { "CMSUser_CMSUserID" });
            DropTable("dbo.CMSUserAreaRights");
            DropTable("dbo.CMSUser");
        }
    }
}

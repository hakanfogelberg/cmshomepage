﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration.Conventions;

using DataAccess.Sql;

namespace DataAccess
{
    public class SqlDBContext : DbContext
    {
        public SqlDBContext() : base ("SqlDBContext")
        {

        }

        public DbSet<Area> Areas { get; set; }
        public DbSet<Sql.Page> Pages { get; set; }
        public DbSet<Paragraph> Paragraphs { get; set; }
        public DbSet<CMSUser> CMSUser { get; set; }
        public DbSet<CMSUserAreaRights> CMSUserAreaRights { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
